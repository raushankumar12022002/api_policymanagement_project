﻿using Microsoft.EntityFrameworkCore;
using PolicyManagement1.Model;

namespace PolicyManagement1.Context
{
    public class PolicyDb:DbContext
    {
        public PolicyDb(DbContextOptions<PolicyDb> db) : base(db)
        {

        }
        public DbSet<AdminPolicy> Policy { get; set; }
    }
}
    
