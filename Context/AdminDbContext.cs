﻿using Microsoft.EntityFrameworkCore;
using PolicyManagement1.Model;

namespace PolicyManagement1.Context
{
    public class AdminDbContext:DbContext
    {
        public AdminDbContext(DbContextOptions<AdminDbContext> db) : base(db)
        {

        }
        public DbSet<Admin> Admins { get; set; }

      

    }
}
