﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PolicyManagement1.Migrations
{
    public partial class MyMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Policy",
                columns: table => new
                {
                    policyId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    policyDiscription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    policyName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    policyCategory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    policyImagePath = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    totalCost = table.Column<int>(type: "int", nullable: false),
                    timeDuration = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Policy", x => x.policyId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Policy");
        }
    }
}
