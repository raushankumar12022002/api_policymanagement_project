﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PolicyManagement1.Exception;
using PolicyManagement1.Model;
using PolicyManagement1.Service;

namespace PolicyManagement1.Controllers

{
    [Authorize]

    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        readonly IAdminService _adminService;
        readonly ITokenGenerator _tokenGenerator;
        public AdminController(IAdminService adminService, ITokenGenerator tokenGenerator)
        {
            _adminService = adminService;
            _tokenGenerator = tokenGenerator;
        }
        [Route("GetAllAdminDetails")]
        [HttpGet]
        public ActionResult GetAllAdmin()
        {
            List<Admin> alluser = _adminService.GetAllAdmin();
            return Ok(alluser);
        }
        [Route("Register")]
        [HttpPost]
        public ActionResult Register(Admin admin)
        {
            bool adduser = _adminService.Register(admin);
            return Ok(adduser);
        }
        
        [Route("Login")]
        [HttpPost]
        public IActionResult AdminLogin(Admin admin)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Admin admin1 = _adminService.AdminLogin(admin);
                    string userToken = _tokenGenerator.GenerateToken(admin.Id, admin.Email);

                    return Ok(userToken);

                }
                else
                {
                    return BadRequest("Please provide all detais");
                }
            }
            catch (AdminCredentialInvalidException ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
        








    }
}
