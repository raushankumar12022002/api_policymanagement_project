﻿using Microsoft.AspNetCore.Mvc;
using PolicyManagement1.Model;
using PolicyManagement1.Service;

namespace PolicyManagement1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PolicyController : ControllerBase
    {
        readonly IPolicyService _policyService;
        public PolicyController(IPolicyService policyService)
        {
            _policyService = policyService;
                    
        }
        [Route("GetAllPolicy")]
        [HttpGet]
        public ActionResult GetAllPolicy()
        {

            List<AdminPolicy> alluser = _policyService.GetAllPolicy();
            return Ok(alluser);
        }
        [Route("DeletePolicy")]
        [HttpDelete]
        public ActionResult DeletePolicy(int id)
        {
            bool deletePolicyStatus = _policyService.DeletePolicy(id);
            return Ok(deletePolicyStatus);
        }
        [Route("AddPolicy")]
        [HttpPost]
        public ActionResult AddPolicy(AdminPolicy adminPolicy)
        {
           
           
           
            if (ModelState.IsValid)
            {

                bool adduser = _policyService.AddPolicy(adminPolicy);
                return Ok(adduser);
            }
            else
            {
                return BadRequest("Please provide details");
            }
        }
        [Route("GetUserById/{id:int}")]
        [HttpGet]
        public ActionResult GetPolicyById(int id)
        {
            AdminPolicy getbyid = _policyService.GetPolicyById(id);
            return Ok(getbyid);
        }
        [HttpPut]
        [Route("EditUser/{id:int}")]
        public ActionResult EditUser(int id, AdminPolicy user)
        {
            bool edituserStatus = _policyService.EditUser(id, user);
            return Ok(edituserStatus);
        }






        }
    }
