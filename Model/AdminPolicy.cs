﻿using System.ComponentModel.DataAnnotations;

namespace PolicyManagement1.Model
{
    public class AdminPolicy
    {
        [Key]
        public int policyId { get; set; }

        public string policyDiscription { get; set; }

        public string policyName { get; set; }
        public string policyCategory { get; set; }
        [DataType(DataType.ImageUrl)]

        public string policyImagePath { get; set; }

        public int totalCost { get; set; }


        public int timeDuration { get; set; }

    }
}
