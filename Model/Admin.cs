﻿using System.ComponentModel.DataAnnotations;

namespace PolicyManagement1.Model
{
    public class Admin
    {
        public int Id { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]


        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]


        public string Password { get; set; }
    }
}
