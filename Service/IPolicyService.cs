﻿using PolicyManagement1.Model;

namespace PolicyManagement1.Service
{
    public interface IPolicyService
    {
        bool AddPolicy(AdminPolicy adminPolicy);
        bool DeletePolicy(int id);
        bool EditUser(int id, AdminPolicy user);
        List<AdminPolicy> GetAllPolicy();
        AdminPolicy GetPolicyById(int id);
    }
}
