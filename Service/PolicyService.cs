﻿using PolicyManagement1.Context;
using PolicyManagement1.Exception;
using PolicyManagement1.Model;
using PolicyManagement1.Repository;

namespace PolicyManagement1.Service
{
    public class PolicyService: IPolicyService
    {
        readonly IPolicyRepository _policyRepository;
        public PolicyService(IPolicyRepository policyRepository)
        {
            _policyRepository = policyRepository;   
        }

        public bool AddPolicy(AdminPolicy adminPolicy)
        {
            var policyprsent = _policyRepository.GetPolicyByName(adminPolicy.policyName);
            if (policyprsent == null)
            {
              bool useradd=  _policyRepository.AddPolicy(adminPolicy);
                if (useradd)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;




            















        }

        public bool DeletePolicy(int id)
        {
            var policyExist = _policyRepository.DeletePolicy(id);
            if (policyExist == null)
            {
                return (bool)_policyRepository.DeletePolicy(id);
            }
            else return false;

        }

        public bool EditUser(int id, AdminPolicy user)
        {
            user.policyId = id;
            int editStatus = _policyRepository.EditUser(user);
            if (editStatus == 1)
            {
                return true;

            }
            else return false;

        }

        public List<AdminPolicy> GetAllPolicy()
        {
            return _policyRepository.GetAllPolicy();
        }

        public AdminPolicy GetPolicyById(int id)
        {
            AdminPolicy policyExist = _policyRepository.GetPolicyId(id);
            if(policyExist != null)
            {
                return policyExist;
            }
            else
            {
                throw new AdminCredentialInvalidException("try again");
            }


        }
    }
}
