﻿using PolicyManagement1.Model;

namespace PolicyManagement1.Service
{
    public interface IAdminService
    {
       
        Admin AdminLogin(Admin admin);
       
        List<Admin> GetAllAdmin();
        
        bool Register(Admin admin);
    }
}
