﻿using PolicyManagement1.Exception;
using PolicyManagement1.Model;
using PolicyManagement1.Repository;

namespace PolicyManagement1.Service
{
    public class AdminService : IAdminService
    {
        readonly IAdminRepository _adminRepository;
        public AdminService(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
                
        }

        

        public Admin AdminLogin(Admin admin)
        {
            Admin rr = _adminRepository.AdminLogin(admin);
            if (rr != null)
            {
                return rr;
            }
            else
            {
                throw new AdminCredentialInvalidException($"UserName and Password is invalid");
            }


        }

        

        public List<Admin> GetAllAdmin()
        {
            return _adminRepository.GetAllAdmin();
        }

        

        public bool Register(Admin admin)
        {
            var AdminExist = _adminRepository.GetAdminById(admin.Id);
            if (AdminExist == null)
            {
                int usertoAdd = _adminRepository.RegisterAdmin(admin);
                if (usertoAdd == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            return false;



        }
    }
}
