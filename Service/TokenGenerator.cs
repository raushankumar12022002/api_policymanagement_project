﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PolicyManagement1.Service
{
    public class TokenGenerator:ITokenGenerator
    {
        public string GenerateToken(int Id, string Email)
        {
            var userClaims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName,Email)

            };
            var userSecurity = Encoding.UTF8.GetBytes("raushan");
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurity);
            var userSigningCredentials = new SigningCredentials(userSymmetricSecurity, SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (issuer: "MVCUserApp",
                audience: "CoreMVcUsers",
                claims: userClaims,
                 expires: DateTime.UtcNow.AddMinutes(10),
                signingCredentials: userSigningCredentials



                );
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            //String userJwt = JsonConvert.SerializeObject(new { Token = userSecurityTokenHandler });
            //return userJwt;

            return userSecurityTokenHandler;








        }


    }
}
