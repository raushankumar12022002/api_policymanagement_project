using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PolicyManagement1.Context;
using PolicyManagement1.Repository;
using PolicyManagement1.Service;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddScoped<IAdminService, AdminService>();
builder.Services.AddScoped<IPolicyService, PolicyService>();
builder.Services.AddScoped<IPolicyRepository, PolicyRepository>();
builder.Services.AddScoped<IAdminRepository, AdminRepository>();
builder.Services.AddScoped<ITokenGenerator, TokenGenerator>();
string localSqlConnectionString2 = "Server=RAUSHANKUMAR;Database=AdminPolicy;Encrypt=True;TrustServerCertificate=True;Trusted_Connection=True";
string localdbConnection = builder.Configuration.GetConnectionString("WebApiConnection");

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddDbContext<AdminDbContext>(o => o.UseSqlServer(localdbConnection));
builder.Services.AddDbContext<PolicyDb>(o => o.UseSqlServer(localSqlConnectionString2));
builder.Services.AddCors();




builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



    

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

}
app.UseCors(x => x
           .AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader()


);



app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
