﻿using PolicyManagement1.Model;

namespace PolicyManagement1.Repository
{
    public interface IAdminRepository
    {
        
        Admin AdminLogin(Admin admin);
       
        Admin GetAdminById(int id);
        List<Admin> GetAllAdmin();
        
        
        int RegisterAdmin(Admin admin);
       
    }
}
