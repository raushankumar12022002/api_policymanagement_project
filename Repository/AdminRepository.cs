﻿using PolicyManagement1.Context;
using PolicyManagement1.Model;
using System.Xml.Linq;

namespace PolicyManagement1.Repository
{
    public class AdminRepository : IAdminRepository
    {
        readonly AdminDbContext _adminDbContext;
        public AdminRepository(AdminDbContext adminDbContext)
        {
            _adminDbContext = adminDbContext;   
                
        }

        
        public Admin AdminLogin(Admin admin)
        {
            return _adminDbContext.Admins.Where(f => f.Email == admin.Email && f.Password == admin.Password).FirstOrDefault();


        }

      
        

        public Admin GetAdminById(int id)
        {
            return _adminDbContext.Admins.Where(u => u.Id == id).FirstOrDefault();
        }

        public List<Admin> GetAllAdmin()
        {
            return _adminDbContext.Admins.ToList();


        }

     
        public int RegisterAdmin(Admin admin)
        {
            _adminDbContext.Admins.Add(admin);
            return _adminDbContext.SaveChanges();


        }


    }
}
