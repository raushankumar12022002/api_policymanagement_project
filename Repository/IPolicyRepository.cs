﻿using PolicyManagement1.Model;

namespace PolicyManagement1.Repository
{
    public interface IPolicyRepository
    {
        bool AddPolicy(AdminPolicy adminPolicy);
        bool DeletePolicy(int id);
        int EditUser(AdminPolicy user);
        List<AdminPolicy> GetAllPolicy();
        AdminPolicy GetPolicyByName(string policyName);
        AdminPolicy GetPolicyId(int id);
    }
}
