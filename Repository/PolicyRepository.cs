﻿using PolicyManagement1.Context;
using PolicyManagement1.Model;

namespace PolicyManagement1.Repository
{
    public class PolicyRepository: IPolicyRepository
    {
        readonly PolicyDb _policyDb;
        public PolicyRepository(PolicyDb policyDb)
        {
            _policyDb = policyDb;

        }

        public bool AddPolicy(AdminPolicy adminPolicy)
        {
            _policyDb.Policy.Add(adminPolicy);
            return _policyDb.SaveChanges() ==1?true:false;


        }

        public bool DeletePolicy(int id)
        {
            AdminPolicy policy = GetPolicyById(id);
            _policyDb.Policy.Remove(policy);
            return _policyDb.SaveChanges() == 1 ? true : false;

        }

        private AdminPolicy GetPolicyById(int id)
        {
            return _policyDb.Policy.Where(x => x.policyId == id).FirstOrDefault();

        }

        public List<AdminPolicy> GetAllPolicy()
        {
            return _policyDb.Policy.ToList();

        }

        public AdminPolicy GetPolicyByName(string policyName)
        {
            return _policyDb.Policy.Where(x => x.policyName == policyName).FirstOrDefault();

        }

        public int EditUser(AdminPolicy user)
        {
            _policyDb.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            return _policyDb.SaveChanges();
        }

        public AdminPolicy GetPolicyId(int id)
        {
            return _policyDb.Policy.Where(x => x.policyId==id).FirstOrDefault();
        }
    }
}
